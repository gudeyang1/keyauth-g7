package role_test

import (
	"testing"

	"gitee.com/go-course/keyauth-g7/apps/role"
)

func TestHasPermission(t *testing.T) {
	set := role.NewRoleSet()

	r := &role.Role{
		Spec: &role.CreateRoleRequest{
			Permissions: []*role.Permission{
				{
					Service:  "cmdb",
					AllowAll: true,
				},
			},
		},
	}

	set.Add(r)

	perm, role := set.HasPermission(&role.PermissionRequest{
		Service:  "cmdb",
		Resource: "secret",
		Action:   "create",
	})

	t.Log(role)
	if perm != true {
		t.Fatal("has perm error")
	}
}
