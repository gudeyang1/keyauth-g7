package impl

import (
	"context"

	"gitee.com/go-course/keyauth-g7/apps/endpoint"

	"github.com/infraboard/mcube/exception"
)

// Save Object
func (s *service) save(ctx context.Context, set *endpoint.EndpiontSet) error {
	// s.col.InsertMany()
	if _, err := s.col.InsertMany(ctx, set.ToDocs()); err != nil {
		return exception.NewInternalServerError("inserted service %s endpoint document error, %s",
			set.Service, err)
	}
	return nil
}
